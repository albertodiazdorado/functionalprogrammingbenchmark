Comparison of three different algorithms for grouping a list of files by size.
* An imperative algorithm using regular `for` loops
* A declarative algorithm using vanilla javascript
* A declarative algorithm using Immutable.js

## Setup
Clone the repository and install dependencies
```bash
git clone https://gitlab.com/albertodiazdorado/functionalprogrammingbenchmark.git
cd functionalprogrammingbenchmark
npm ci
```
If you want to use real test data, install it via
```bash
npm run download_test_data
```

## Usage

### Real test data
The folder `testData` contains different npm projects.
Different dependencies are installed in each project, resulting in a different number of files in the node_modules folder of each project.

Install the test data via `npm run download_test_data`. Then, select the node_modules folder you want to use as a benchmark by modifying the variable `testData` in `test-real-data.js`.
Run the benchmark using:
```bash
npm run test-real-data
```
You can observe that the vanilla javascript algorithm execution time grows quadratic with the number of files in the sample.
The other two algorithms grow just linearly with the number of files.

### Random test data
Alternatively, you can generate random test data for the benchmark.
To do so, edit the variables `numberOfFiles` and `maximumFileSize` in `test-random-data.js`, then execute the benchmark by running:
```bash
npm run test-random-data
```
You can observe that the execution time of the three algorithms grows linearly in the parameter `numberOfFiles`, whereas vanilla javascript is the only one that grows linearly in the second parameter `maximumFileSize`.