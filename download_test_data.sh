#!/usr/bin/env bash

pushd testData

for d in $(ls)
do
    pushd ${d}
    npm ci
    popd
done

popd