const {generateRandomFilesData} = require("./src/generate-random-files-data");
const {benchmark} = require("./src/benchmark");

const numberOfFiles = 1000;
const maximumFileSize = 10000;

benchmark(generateRandomFilesData(numberOfFiles, maximumFileSize));