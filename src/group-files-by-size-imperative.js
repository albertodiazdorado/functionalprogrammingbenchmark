const groupFilesBySizeImperative = (allFileData) => {
  const filesSortedBySize = {};
  for (const fileData of allFileData) {
    if (fileData.size in filesSortedBySize) {
      filesSortedBySize[fileData.size].push(fileData.file);
    } else {
      filesSortedBySize[fileData.size] = [fileData.file];
    }
  }

  return filesSortedBySize;
};

exports.groupFilesBySizeImperative = groupFilesBySizeImperative;
