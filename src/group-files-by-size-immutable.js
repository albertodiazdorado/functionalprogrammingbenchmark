const { Map, List } = require("immutable");

const groupFilesBySizeImmutable = (allFileData) =>
  allFileData.reduce(
    (filesSortedBySize, { size, file }) =>
      filesSortedBySize.update(size, List(), (list) => list.push(file)),
    Map()
  );

exports.groupFilesBySizeImmutable = groupFilesBySizeImmutable;
