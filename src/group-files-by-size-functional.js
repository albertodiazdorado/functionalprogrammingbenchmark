const groupFilesBySizeFunctional = (allFileData) =>
  allFileData.reduce(
    (filesSortedBySize, fileData) => ({
      ...filesSortedBySize,
      [fileData.size]: filesSortedBySize[fileData.size]
        ? [...filesSortedBySize[fileData.size], fileData.file]
        : [fileData.file],
    }),
    {}
  );

exports.groupFilesBySizeFunctional = groupFilesBySizeFunctional;
