const fs = require("fs").promises;
const path = require("path");

const walk = async (dir) => {
  let files;
  files = await fs.readdir(dir);
  const parsedFiles = await Promise.all(
    files.map(async (fileName) => {
      const filePath = path.join(dir, fileName);

      const stats = await fs.lstat(filePath);
      if (stats.isSymbolicLink() || stats.size === 0) {
        return null;
      }
      if (stats.isDirectory()) {
        return walk(filePath);
      } else if (stats.isFile()) {
        return { file: filePath, size: stats.size };
      }
    })
  );

  return parsedFiles.reduce(
    (all, folderContents) =>
      folderContents ? all.concat(folderContents) : all,
    []
  );
};

exports.walk = walk;
