const generateRandomFilesData = (numberOfFiles, maximumFileSize) => {
  const allFileData = new Array(numberOfFiles);

  const makeId = (length) => {
    let result = "";
    const characters =
      "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    const charactersLength = characters.length;
    for (let idx = 0; idx < length; idx++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return result;
  };

  for (let idx = 0; idx < allFileData.length; idx++) {
    const fileNameLength = Math.floor(Math.random() * 10);
    allFileData[idx] = {
      file: makeId(fileNameLength),
      size: Math.floor(Math.random() * maximumFileSize),
    };
  }

  return allFileData;
};

exports.generateRandomFilesData = generateRandomFilesData;
