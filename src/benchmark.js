const {
  groupFilesBySizeImmutable,
} = require("./group-files-by-size-immutable");
const {
  groupFilesBySizeFunctional,
} = require("./group-files-by-size-functional");
const {
  groupFilesBySizeImperative,
} = require("./group-files-by-size-imperative");

const benchmark = (allFileData) => {
  console.log(`Total files: ${allFileData.length}`);

  let start;

  start = new Date();
  const result1 = groupFilesBySizeImperative(allFileData);
  const time1 = new Date() - start;

  start = new Date();
  const result2 = groupFilesBySizeFunctional(allFileData);
  const time2 = new Date() - start;

  start = new Date();
  const result3 = groupFilesBySizeImmutable(allFileData);
  const time3 = new Date() - start;

  console.log("\nFINAL REPORT:");
  console.log(
    `Are results equal? ${
      JSON.stringify(result1) === JSON.stringify(result2) &&
      JSON.stringify(result1) === JSON.stringify(result3)
    }`
  );
  console.log(`Non functional approach: ${time1} ms`);
  console.log(`Functional approach: ${time2} ms`);
  console.log(`Immutable approach: ${time3} ms`);
};

exports.benchmark = benchmark;
