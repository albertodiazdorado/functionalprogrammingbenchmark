const path = require("path");
const {benchmark} = require("./src/benchmark");
const {walk} = require("./src/walk");

const testDataOptions = {
  IMMUTABLE_PRETTIER: "testData/immutablePrettier/node_modules", //    31 files
  LODASH: "testData/lodash/node_modules", //  1049 files
  ESLINT: "testData/eslint/node_modules", //  6288 files
  MATERIAL_DESIGN_ICONS: "testData/materialDesignIcons/node_modules", // 89814 files
};
const testData = testDataOptions.ESLINT;

const dir = path.resolve(testData);
walk(dir).then(benchmark);
